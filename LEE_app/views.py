from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from LEE_pro import settings
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

# import streamlit as st
# import streamlit.components.v1 as components
# from PIL import Image
import os
import tika

tika.initVM()
from tika import parser

import cv2
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from skimage import measure, morphology
from skimage.color import label2rgb
from skimage.measure import regionprops
import pdf2image
import pickle
import getopt, sys

import re
# import numpy as np
import lexnlp
import spacy
import lexnlp.extract.en.entities.nltk_re
import lexnlp.extract.en.dates
from spacy.lang.en.stop_words import STOP_WORDS
# import pdf2image
import lexnlp.extract.en.entities.nltk_maxent
import lexnlp.extract.en.entities.nltk_re
from collections import Counter
import math
from nltk import sent_tokenize
nlp = spacy.load("en_core_web_sm")
stopwords = list(STOP_WORDS)
from string import punctuation

punctuation = punctuation + '\n'

path = settings.MEDIA_ROOT


@csrf_exempt
def home(request):
    """
    :param request:
    :return:
    """
    try:
        files, filename = file_system(request)
        text = extract_text(files)

        result_dict, doc_dict = doc_wit_regex(text)
        keys = ['date', 'validity', 'vendor_name', 'vendor_designation',
                'first party', 'second party', 'amount', 'party_name', 'party_designation']
        new_dict = {key: result_dict[key] for key in keys}

        pkl_file = pickle.load(open(path + '/template_doc.pkl','rb'))

        changes = change_flags(doc_dict, pkl_file)

        return render(request, 'home.html', {
            "filename": filename,
            "text": text,
            "form_text": new_dict,
            "summary": result_dict['summary'],
            "changes": changes
        })
    except Exception as e:
        print("Exception", e)

    return render(request, 'home.html', {})


def file_system(request):
    """

    :return:
    """
    if request.method == 'POST':
        myfile = request.FILES['files']
        test = os.listdir(path)
        for item in test:
            if item.endswith(".pdf") or item.endswith(".png"):
                os.remove(os.path.join(path, item))

        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        files = path + '/' + filename
        sign_img = extract_sign(files)
        plt.imsave(path + '/sign_img.png', sign_img)
        return files, filename


def extract_text(file):
    raw = parser.from_file(file)
    text = raw['content']
    #     print(text)
    text = text.strip()
    text = [re.sub(' +', ' ', temp) for temp in text]
    text = [re.sub('\n +', '\n', temp) for temp in text]
    text = [re.sub('\n+', '\n', temp) for temp in text]
    return "".join(text)


def doc_wit_regex(doc_text):
    final_dict = dict()
    text = doc_text.replace('\n', '')
    date_sent = re.findall('professional services agreement(.*)by and between', text, re.IGNORECASE)
    company_sent = re.search('by and between(.*)now this agreement witnesseth', text, re.IGNORECASE).group(1)
    scope_sent = re.search('scope of the agreement(.*)professional fees, billing', text, re.IGNORECASE).group(1)
    prof_sent = re.search('professional fees, billing(.*)not employment', text, re.IGNORECASE).group(1)
    emp_sent = re.search('not employment(.*)no obligations on part of', text, re.IGNORECASE).group(1)
    nob_sent = re.search('no obligations on part of(.*)non compete', text, re.IGNORECASE).group(1)
    non_compete_sent = re.search('non compete(.*)non-solicitation', text, re.IGNORECASE).group(1)
    non_soli_sent = re.search('non-solicitation(.*)termination of the contract', text, re.IGNORECASE).group(1)
    termination_sent = re.search('termination of the contract(.*)force majeure', text, re.IGNORECASE).group(1)
    force_maj_sent = re.search('force majeure(.*)law./.jurisdiction./.arbitration', text, re.IGNORECASE).group(1)
    law_juri_sent = re.search('law./.jurisdiction./.arbitration(.*)alterations/.modifications', text,
                              re.IGNORECASE).group(1)
    alter_mod_sent = re.search('alterations/.modifications(.*)indemnity', text, re.IGNORECASE).group(1)

    indemnity_sent = re.search('indemnity(.*)use of business cards', text, re.IGNORECASE).group(1)
    use_business_sent = re.search('use of business cards(.*)validity of the agreement & rates', text,
                                  re.IGNORECASE).group(1)
    validity_agr_sent = re.search('validity of the agreement & rates(.*)confidentiality', text, re.IGNORECASE).group(1)
    confident_sent = re.search(' confidentiality(.*)others', text, re.IGNORECASE).group(1)
    others_sent = re.search('. Others(.*)In witness whereof, the parties', text).group(1)
    rest_sent = re.search('(in witness whereof, the parties.*)', text, re.IGNORECASE).group(1)
    #     summary_sent = scope_sent+prof_sent
    #     print(summary_sent,'11111111111111111111111')
    final_dict['scope'] = scope_sent
    final_dict['prof_sent'] = prof_sent
    final_dict['non_employment'] = emp_sent
    final_dict['no_obligations'] = nob_sent
    final_dict['non_compete'] = non_compete_sent
    final_dict['non_solicitation'] = non_soli_sent
    final_dict['contract_termin'] = termination_sent
    final_dict['force_majeure'] = force_maj_sent
    final_dict['law_jurisdiction'] = law_juri_sent
    final_dict['alter_modifiy'] = alter_mod_sent

    final_dict['indemnity'] = indemnity_sent
    final_dict['use_business_card'] = use_business_sent
    final_dict['validity_agreement'] = validity_agr_sent
    final_dict['doc_misc'] = others_sent

    final_dict['date'] = list(lexnlp.extract.en.dates.get_dates(str(date_sent)))[0].strftime('%d-%m-%Y')
    company_sent = re.sub('   ', ' ', company_sent)
    company_sent = company_sent.lstrip()
    comp_doc = nlp(company_sent)
    # print(111,company_sent,1111)
    # for entity in comp_doc.ents:
    #     print(entity.label_,"||",entity.text)
    companies = [entity.text for entity in comp_doc.ents if entity.label_ == 'ORG'][:2]

    final_dict['first party'] = companies[0]
    final_dict['second party'] = companies[1]
    final_dict['amount'] = ". ".join(re.findall('(?<=)rs. \d[^.]*|(?<=)USD[^.]*', prof_sent, re.IGNORECASE))
    # print(rest_sent)
    vendor_name = re.findall('name(.*)name', rest_sent, re.IGNORECASE)
    if vendor_name:
        final_dict['vendor_name'] = vendor_name[0].replace(':', '')
    else:
        final_dict['vendor_name'] = vendor_name
    designation = re.findall('designation(.*)designation', rest_sent, re.IGNORECASE)
    if designation:
        final_dict['vendor_designation'] = designation[0].replace(":", '')
    else:
        final_dict['vendor_designation'] = designation
    party_name = re.findall('name.*?name(.*?)designation', rest_sent, re.IGNORECASE)
    if party_name:
        final_dict['party_name'] = party_name[0].replace(":", '')
    else:
        final_dict['party_name'] = party_name
    party_designation = re.findall('designation.*?designation(.*?)date', rest_sent, re.IGNORECASE)
    if party_designation:
        final_dict['party_designation'] = party_designation[0].replace(":", '')
    else:
        final_dict['party_designation'] = party_designation

    final_dict['validity'] = []
    if list(lexnlp.extract.en.dates.get_dates(str(validity_agr_sent))):
        final_dict['validity'] = list(lexnlp.extract.en.dates.get_dates(str(validity_agr_sent)))[0].strftime('%d-%m-%Y')

    f_sum = re.search('professional services agreement.*by and between', text, re.IGNORECASE).group(0)
    final_dict['summary'] = f_sum + " " + " and ".join(companies[:2]) + " with billing of " + final_dict['amount']

    policies = {'Scope of Agreement': scope_sent, 'Professional and Billing': prof_sent, 'Non Employment': emp_sent,
                'No-Obligations': nob_sent,
                'Non-Compete': non_compete_sent, 'Non-Solicitation': non_soli_sent,
                'Termination of the Contract': termination_sent,
                'Force Mejeure': force_maj_sent, 'Law and jurisdiction': law_juri_sent,
                'Alterations-Modifications': alter_mod_sent,
                'Indemnity': indemnity_sent, 'Use of Business Cards': use_business_sent,
                'Confidentiality': confident_sent, 'Others': others_sent}

    return final_dict, policies


def extract_sign(pdf_file):
    pages = pdf2image.convert_from_path(pdf_file)
    rev_pages = pages[::-1]
    ######removing Horizontal and Vertical Lines
    for page in rev_pages:
        page.save('out_s.jpg')
        orginalImg = cv2.imread('out_s.jpg')
        result = orginalImg.copy()

        gray = cv.cvtColor(orginalImg, cv.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40, 1))
        remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
        cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(result, [c], -1, (255, 255, 255), 5)

        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 40))
        remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
        cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]

        for c in cnts:
            cv2.drawContours(result, [c], -1, (255, 255, 255), 5)

        originalGray = cv.cvtColor(result, cv.COLOR_BGR2GRAY)
        img = cv.threshold(originalGray, 127, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)[1]

        scale_percent = 50

        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)

        resized = cv.resize(img, (width, height))

        # cv.imshow('resized thresh image', resized)

        # looking for connected components
        # https://scipy-lectures.org/packages/scikit-image/auto_examples/plot_labels.html
        blobs = img > img.mean()
        blobs_labels = measure.label(blobs, background=1)
        # image_label_overlay = label2rgb(blobs_labels, image=img)
        regions = regionprops(blobs_labels)
        totalArea = 0
        count = 0
        for region in regions:
            if (region.area > 10):
                totalArea += region.area
                count += 1

        minSize = (((totalArea / count) / 75) * 280) + 85

        smallObjectsRemoved = morphology.remove_small_objects(blobs_labels, min_size=minSize, connectivity=1,
                                                              in_place=False)
        plt.imsave('before_final.png', smallObjectsRemoved)

        before_final = cv.imread('before_final.png', 0)

        finalImg = cv.threshold(before_final, 0, 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]

        ############################ SIGNATURE CROP ############################
        retval, thresh_gray = cv2.threshold(finalImg, thresh=100, maxval=255, type=cv2.THRESH_BINARY)

        points = np.argwhere(thresh_gray == 0)  # find where the black pixels are
        points = np.fliplr(points)  # store them in x,y coordinates instead of row,col indices
        x, y, w, h = cv2.boundingRect(points)  # create a rectangle around those points
        x, y, w, h = x - 10, y - 10, w + 20, h + 20  # make the box a little bigger
        crop = gray[y:y + h, x:x + w]  # create a cropped region of the gray image

        # get the thresholded crop
        retval, thresh_crop = cv2.threshold(crop, thresh=200, maxval=255, type=cv2.THRESH_BINARY)
        if np.mean(thresh_crop) == 255:
            continue

        else:
            break
    return thresh_crop


def change_flags(document_dict, temp_file):
    doc_clean = {k: [i.strip() for i in sent_tokenize(v)] for k, v in document_dict.items()}
    temp_file_clean = {k: [i.strip() for i in sent_tokenize(v)] for k, v in temp_file.items()}
    changes = dict()
    f_file = dict()
    missing_headers = [k for k in temp_file.keys() if k not in document_dict.keys()] or None
    for k, t_val in doc_clean.items():
        for text in t_val:
            text1 = text_to_vector(text)
            sc_lst = [text_to_vector(i) for i in temp_file_clean[k]]
            for sc in sc_lst:
                cosine = get_cosine(text1, sc)
                changes.setdefault((k, text), []).append(cosine)
    changes = {k: max(v) for k, v in changes.items() if v is not None}
    for change, score in changes.items():
        if score < 0.45:
            f_file.setdefault(change[0], []).append(change[1])

    f_file['missing_headers'] = missing_headers

    f_file = {k: ' '.join(v) for k, v in f_file.items() if v is not None}

    return f_file


WORD = re.compile(r"\w+")


def get_cosine(vec1, vec2):
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x] ** 2 for x in list(vec1.keys())])
    sum2 = sum([vec2[x] ** 2 for x in list(vec2.keys())])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(text):
    words = WORD.findall(text)
    return Counter(words)


