from django.apps import AppConfig


class LeeAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'LEE_app'
